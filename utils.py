"""
Инструменты, хелперы и т.п.
"""
import functools
import os
import random
import shutil
from config import SAMPLES_PATH, TESTS_PATH


class SampleAndTestGenerator(object):
    """
    Делим данные на обучающую и тестовую выборки
    """

    @classmethod
    def clear_data_dirs(cls, set_name):
        """
        Очищаем прежний набор данных
        """
        samples_path = os.path.join(SAMPLES_PATH, set_name)
        tests_path = os.path.join(TESTS_PATH, set_name)

        if os.path.exists(samples_path):
            shutil.rmtree(samples_path)
        if os.path.exists(tests_path):
            shutil.rmtree(tests_path)

    @classmethod
    def separate(cls, source_path: str):
        """
        :param source_path: путь до данных
        в данной папке будут созданны подпапки sample и tests с симлинками на нужные набор данных
        """

        dirname = os.path.dirname(source_path)
        set_name = os.path.split(dirname)[-1]
        cls.clear_data_dirs(set_name)

        dirnames = os.listdir(source_path)

        for dataset in dirnames:
            set_data = os.listdir(os.path.join(dirname, dataset))
            sample_path = os.path.join(SAMPLES_PATH, set_name, dataset)
            test_path = os.path.join(TESTS_PATH, set_name, dataset)
            if not os.path.exists(sample_path):
                os.makedirs(sample_path)
            if not os.path.exists(test_path):
                os.makedirs(test_path)

            for elem in set_data:
                src = os.path.join(dirname, dataset, elem)
                src = os.path.abspath(src)
                if random.randint(0, 5):
                    link = os.path.join(test_path, elem)
                else:
                    link = os.path.join(sample_path, elem)
                os.symlink(src, link)
                pass
            pass
        pass
