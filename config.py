import os

BASE_PATH = os.path.dirname(__file__)
LOG_FILE = os.path.join(BASE_PATH, 'logs', 'log.txt')
DATA = os.path.join(BASE_PATH, 'data')
SAMPLES_PATH = os.path.join(DATA, 'samples')
TESTS_PATH = os.path.join(DATA, 'test')