from collections import defaultdict
import os
import numpy as np
import pylab
from scipy.sparse import rand
from config import SAMPLES_PATH
from pandas import DataFrame


class DriverPreHandler(object):
    """
    Подготовка данных к машинному обучению
    :type data: dict of dict
    :type data_properties: dict of dict
    """

    def __init__(self, set_name: str):
        self.set_name = set_name
        self.data = defaultdict(dict)
        self.data_properties = defaultdict(dict)


    def visualisation_properties(self):
        for driver in self.data_properties:
            plot_data = []
            keys = ['avg_speed', 'max_speed', 'avg_braking', 'max_braking', 'avg_acc', 'max_acc']

            for trip in sorted(self.data_properties[driver].keys()):
                trip_props = self.data_properties[driver][trip]
                prop_values = [trip_props[key] for key in keys]
                plot_data.append(prop_values)

            df2 = DataFrame(plot_data, columns=keys)
            df2.plot(kind='bar')
            pylab.show()
            print('should_show')

    @staticmethod
    def eval_trip_properties(trip_data: np.ndarray) -> dict:
        """
        Вычисляет для трека характеристики, такие как средняя скорость, среднее ускорение разгона/торможения...
        :param trip_data:  ndarray with dtype: [x, y, xd, yd, speed, acceleration]
        """
        trip_data.dtype = float
        speeds = trip_data[:, 4]
        avg_speed = np.average(speeds)
        max_speed = np.max(speeds)
        acc = [val for val in trip_data[:, 5] if val > 0]
        braking = [abs(val) for val in trip_data[:, 5] if val < 0]
        avg_acc = np.mean(acc)
        avg_braking = np.mean(braking)
        max_acc = np.max(acc)
        max_braking = np.max(braking)

        return {
            'avg_speed': avg_speed,
            'avg_acc': avg_acc,
            'avg_braking': avg_braking,
            'max_speed': max_speed,
            'max_acc': max_acc,
            'max_braking': max_braking,
        }

    def eval_data_properties(self):
        """
        Вычисляет для каждого трека характеристики, такие как средняя скорость, среднее ускорение разгона/торможения...
        """
        for driver in self.data:
            for trip in self.data[driver]:
                trip_data = self.data[driver][trip]

                self.data_properties[driver][trip] = self.eval_trip_properties(trip_data)

    @staticmethod
    def mark_trip(trip_data: np.ndarray) -> np.ndarray:
        """
        Добавляет к каждой точке в данных метаинформацию - смещение, скорость, ускорение
        """
        rows, cols = trip_data.shape
        pass
        pass
        xy_diff = np.diff(trip_data, axis=0)
        # на самом деле мы не знаем последней разницы, но представим что скорость не изменилась
        xy_diff = np.vstack([xy_diff, xy_diff[-1]])
        speeds = [np.linalg.norm(diff) for diff in xy_diff]
        speeds = np.array(speeds)
        speeds.shape = (rows, 1)

        accelerations = np.diff(speeds, axis=0)
        # как уже говорилось, представим что ускорение в последней точки было равно нулю
        accelerations = np.vstack([accelerations, np.array([0])])
        trip_data = np.hstack([trip_data, xy_diff, speeds, accelerations])
        trip_data.dtype = [('x', float), ('y', float), ('xd', float), ('yd', float), ('speed', float), ('acs', float)]

        return trip_data

    def mark_data(self):
        """
        Добавляет к каждой точке в данных метаинформацию - смещение, скорость, ускорение
        """
        for driver in self.data:
            for trip in self.data[driver]:
                trip_data = self.data[driver][trip]

                self.data[driver][trip] = self.mark_trip(trip_data)

    def load_samples(self):
        samples_path = os.path.join(SAMPLES_PATH, self.set_name)
        datasets = os.listdir(samples_path)
        for driver in datasets:
            set_path = os.path.join(samples_path, driver)
            set_data = os.listdir(set_path)
            for trip in set_data:
                trip_path = os.path.join(set_path, trip)
                trip_data = np.loadtxt(trip_path, delimiter=',', skiprows=1)
                self.data[driver][trip] = trip_data
            # break  # debug
        pass


class Learn(object):
    @classmethod
    def learn(cls, set_name):
        pre_handler = DriverPreHandler(set_name)
        pre_handler.load_samples()
        pre_handler.mark_data()
        pre_handler.eval_data_properties()
        # pre_handler.visualisation_properties()
        pass
