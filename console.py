import argparse
import os
import sys
from config import BASE_PATH
from learning import Learn

sys.path.append(BASE_PATH)


def handle_parser(args_: argparse.Namespace):
    if args_.task == 'makeset':
        from utils import SampleAndTestGenerator

        SampleAndTestGenerator.separate(args_.file)


def handle_learn(args_: argparse.Namespace):
    Learn.learn(args_.set_name)

if __name__ == "__main__":
    sys.path.append(os.path.abspath(os.path.dirname(__file__)))
    parser = argparse.ArgumentParser(prog='Machine Learning console tools')

    parser.add_argument('--foo', action='store_true', help='foo help')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_parser = subparsers.add_parser('parser', help='Handle files: parse, separate or something else')
    parser_parser.add_argument('task', type=str, help='set task name', choices=('makeset', ))
    parser_parser.add_argument('file', type=str, help='Parse file')
    parser_parser.set_defaults(handler=handle_parser)

    learn_parser = subparsers.add_parser('learn', help='Зerforms machine learning')
    learn_parser.add_argument('set_name', type=str, help='Set_name is usually the name of the subdirectory in samples')
    learn_parser.set_defaults(handler=handle_learn)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()  # INFO: because you should provider one of subparsers argument
    else:
        args = parser.parse_args()
        args.handler(args)
